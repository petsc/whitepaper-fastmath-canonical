PDFLATEX = pdflatex
TARGETS  = paper.pdf
SOURCE   = paper.tex
BIBSRC = petsc.bib petscapp.bib
DEPS     = ${BIBSRC}
TEX_SUFS = .aux .log .nav .out .snm .toc .tdo .vrb

all: ${TARGETS}

petsc.bib:
	if [ -f ${PETSC_DIR}/src/docs/tex/$@ ]; then ln -s ${PETSC_DIR}/src/docs/tex/$@ $@; else curl -o $@ "https://bitbucket.org/petsc/petsc/raw/HEAD/src/docs/tex/$@?at=master"; fi
petscapp.bib:
	if [ -f ${PETSC_DIR}/src/docs/tex/$@ ]; then ln -s ${PETSC_DIR}/src/docs/tex/$@ $@; else curl -o $@ "https://bitbucket.org/petsc/petsc/raw/HEAD/src/docs/tex/$@?at=master"; fi

%.pdf	: %.tex ${DEPS}
	${PDFLATEX} $<
	BSTINPUTS=${BSTINPUTS}:./styles BIBINPUTS=${BIBINPUTS}:${PETSC_DIR}/src/docs/tex:${HOME}/Documents bibtex $(subst .tex, , ${<})
	${PDFLATEX} $<
	${PDFLATEX} $<


clean:
	rm $(foreach suf, ${TEX_SUFS}, *${suf}) || true

cleaner: clean
	rm ${TARGETS}
